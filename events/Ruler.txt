namespace = anb_ruler

# Any culture that lives longer 120 years max should be given this flag
country_event = {
	id = anb_ruler.0
	title = anb_ruler.0.t
	desc = anb_ruler.0.d
	picture = DIPLOMACY_eventPicture
	
    trigger = {
		has_dlc = "Rights of Man"
		months_of_ruling = 1
		OR = {
			ruler_is_elven = yes
			ruler_is_dwarven = yes
			ruler_is_gnomish = yes
			#or lich
        }
		NOT = { ruler_has_personality = immortal_personality } 
        NOT = { has_ruler_flag = set_immortality }
    }
	
	immediate = {
		hidden_effect = {
			if = {
				limit = { ruler_has_max_personalities = yes }
				clear_scripted_personalities = yes
			}
			add_ruler_personality = immortal_personality
			set_ruler_flag = set_immortality
		}
	}
	option = {
		name = anb_ruler.0.a
	}
}

# Death event
# When a ruler has an Immortal flag, and is of a culture with a long, but not immortal lifespan, this event will strip them of the flag up when they reach the "Old" age for their culture
country_event = {
	id = anb_ruler.1
	title = anb_ruler.1.t
	desc = {
		trigger = { ruler_is_dwarven = yes }
		desc = anb_ruler.1.dwarven
	}
	desc = {
		trigger = { ruler_is_gnomish = yes }
		desc = anb_ruler.1.gnomish
	}
	desc = {
		trigger = { ruler_is_elven = yes }
		desc = anb_ruler.1.elven
	}
	picture = DIPLOMACY_eventPicture
	
	mean_time_to_happen = {
		months = 3
	}
	
    trigger = {
        has_dlc = "Rights of Man"
        ruler_has_personality = immortal_personality
		NOT = { has_ruler_flag = end_life }
        OR = {
            # Dwarfs
            AND = {
                OR = {
                    ruler_is_dwarven = yes
                }
                ruler_age = 160 #200 Expectancy
            }
            # Gnomes
            AND = {
                ruler_is_gnomish = yes
                ruler_age = 210 #250 Expectancy
            }
            # Elves
            AND = {
                ruler_is_elven = yes
                ruler_age = 350 #400 Expectancy
            }
        }
		#Future Additions: Liches
    }
   
	option = {
		name = anb_ruler.1.a
		
		hidden_effect = { set_ruler_flag = end_life }
		if = {
			limit = { ruler_is_dwarven = yes }
			custom_tooltip = ruler_is_dwarven_tooltip
			hidden_effect = { country_event = { id = anb_ruler.4 days = 10950 random = 7300 } } #30 years + ~20years
		}
		else_if = {
			limit = { ruler_is_gnomish = yes }
			custom_tooltip = ruler_is_gnomish_tooltip
			hidden_effect = { country_event = { id = anb_ruler.4 days = 12775 random = 7300 } } #35 years + ~20years
		}
		else_if = {
			limit = { ruler_is_elven = yes }
			custom_tooltip = ruler_is_elven_tooltip
			hidden_effect = { country_event = { id = anb_ruler.4 days = 14600 random = 7300 } } #40 years + ~20years
		}
	}
}
#Immortal ruler die
country_event = {
	id = anb_ruler.4
	title = anb_ruler.4.t
	desc = anb_ruler.4.d
	picture = DIPLOMACY_eventPicture
	
	is_triggered_only = yes
	
    trigger = {
        has_ruler_flag = end_life
    }
   
	option = {
		name = anb_ruler.4.a
		
        kill_ruler = yes
	}
}

# Age Decadence
country_event = {
	id = anb_ruler.2
	title = anb_ruler.2.t
	desc = anb_ruler.2.d
	picture = DIPLOMACY_eventPicture
	
    trigger = {
        has_dlc = "Rights of Man"
        ruler_has_personality = immortal_personality
		ruler_age = 100
    }
	
	#is_triggered_only = yes
	
	mean_time_to_happen = {
		months = 240
		modifier = {
			factor = 0.9
			ruler_age = 200
		}
		modifier = {
			factor = 0.8
			ruler_age = 250
		}
		modifier = {
			factor = 0.7
			ruler_age = 300
		}
		modifier = {
			factor = 0.5
			ruler_age = 350
		}
		
		modifier = {
			factor = 0.9
			ruler_has_personality = careful_personality 
		}
		modifier = {
			factor = 0.9
			ruler_has_personality = zealot_personality 
		}
		modifier = {
			factor = 1.5
			ruler_has_personality = free_thinker_personality 
		}
		modifier = {
			factor = 1.2
			ruler_has_personality = scholar_personality 
		}
	}
   
	option = {	#Am I out of touch? No, it's the younger races who are wrong.
		name = anb_ruler.2.a
		if = {
			limit = {
				OR = {
					NOT = { adm = 0}
					NOT = { dip = 0}
					NOT = { mil = 0}
				}
			}
		}
		
		random_list = {
			30 = {
				decrease_ruler_adm_effect = yes
			}
			30 = {
				decrease_ruler_dip_effect = yes
			}
			30 = {
				decrease_ruler_mil_effect = yes
			}
			10 = {
				#Nothing make a tooltip
			}
		}
	}
    
    option = {	#Reminisce about the glory days
		name = anb_ruler.2.b
		if = {
			limit = {
				NOT = { prestige = -80}
			}
		}
		
        add_prestige = -45	#may be a bit too high
	}
    
    option = {	#The world moves too fast... I must contemplate on this.
		name = anb_ruler.2.c
		if = {
			limit = {
				NOT = { stability = -3}
			}
		}
		
        add_stability = -3
	}
}

# Age Revival
country_event = {
	id = anb_ruler.3
	title = anb_ruler.3.t
	desc = anb_ruler.3.d
	picture = DIPLOMACY_eventPicture
	
    trigger = {
        has_dlc = "Rights of Man"
        ruler_has_personality = immortal_personality
		#culture_group = elven
		ruler_age = 100
    }
	
	#is_triggered_only = yes
	
	mean_time_to_happen = {
		months = 140
		modifier = {
			factor = 0.8
			ruler_has_personality = free_thinker_personality 
		}
		modifier = {
			factor = 0.9
			ruler_has_personality = scholar_personality 
		}
		# modifier = {
			# factor = 1.5
			# ruler_age = 250
		# }
		# modifier = {
			# factor = 2
			# ruler_age = 300
		# }
		# modifier = {
			# factor = 3
			# ruler_age = 350
		# }
	}
   
	option = {	#We must be open to new change and ideas.
		name = anb_ruler.3.a
		
		increase_ruler_adm_effect = yes
	}
    
    option = {	#There's a whole new world out there, as an elder race we must lead the way!
		name = anb_ruler.3.b
		
		increase_ruler_dip_effect = yes
	}
    
    option = {	#I've seen enough war to know where we're headed.
		name = anb_ruler.3.c
		
        increase_ruler_mil_effect = yes
	}
}

