government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
primary_culture = khedarid
religion = high_philosophy
technology_group = tech_raheni
religious_school = unbroken_claw_school
capital = 4465

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }