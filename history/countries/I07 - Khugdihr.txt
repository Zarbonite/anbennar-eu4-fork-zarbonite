government = republic
add_government_reform = adventurer_republic_reform
government_rank = 1
primary_culture = agate_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
capital = 2863
fixed_capital = 2863

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }